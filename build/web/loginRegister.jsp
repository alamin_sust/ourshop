<%-- 
    Document   : loginRegister
    Created on : Apr 26, 2019, 10:47:33 AM
    Author     : md_al
--%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.ourshop.connection.Database"%>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Rent Awesome Cars</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="loginRegister/maxcdn.bootstrapcdn.com_bootstrap_3.3.7_css_bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="loginRegister/maxcdn.bootstrapcdn.com_bootstrap_3.3.7_js_bootstrap.min.js"></script>
        <script src="loginRegister/code.jquery.com_jquery-1.11.1.min.js"></script>
        <!------ Include the above in your HEAD tag ---------->
        <style>
            body {
                padding-top: 90px;
            }
            .panel-login {
                border-color: #ccc;
                -webkit-box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2);
                -moz-box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2);
                box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2);
            }
            .panel-login>.panel-heading {
                color: #00415d;
                background-color: #fff;
                border-color: #fff;
                text-align:center;
            }
            .panel-login>.panel-heading a{
                text-decoration: none;
                color: #666;
                font-weight: bold;
                font-size: 15px;
                -webkit-transition: all 0.1s linear;
                -moz-transition: all 0.1s linear;
                transition: all 0.1s linear;
            }
            .panel-login>.panel-heading a.active{
                color: #029f5b;
                font-size: 18px;
            }
            .panel-login>.panel-heading hr{
                margin-top: 10px;
                margin-bottom: 0px;
                clear: both;
                border: 0;
                height: 1px;
                background-image: -webkit-linear-gradient(left,rgba(0, 0, 0, 0),rgba(0, 0, 0, 0.15),rgba(0, 0, 0, 0));
                background-image: -moz-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
                background-image: -ms-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
                background-image: -o-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
            }
            .panel-login input[type="text"],.panel-login input[type="email"],.panel-login input[type="password"] {
                height: 45px;
                border: 1px solid #ddd;
                font-size: 16px;
                -webkit-transition: all 0.1s linear;
                -moz-transition: all 0.1s linear;
                transition: all 0.1s linear;
            }
            .panel-login input:hover,
            .panel-login input:focus {
                outline:none;
                -webkit-box-shadow: none;
                -moz-box-shadow: none;
                box-shadow: none;
                border-color: #ccc;
            }
            .btn-login {
                background-color: #59B2E0;
                outline: none;
                color: #fff;
                font-size: 14px;
                height: auto;
                font-weight: normal;
                padding: 14px 0;
                text-transform: uppercase;
                border-color: #59B2E6;
            }
            .btn-login:hover,
            .btn-login:focus {
                color: #fff;
                background-color: #53A3CD;
                border-color: #53A3CD;
            }
            .forgot-password {
                text-decoration: underline;
                color: #888;
            }
            .forgot-password:hover,
            .forgot-password:focus {
                text-decoration: underline;
                color: #666;
            }

            .btn-register {
                background-color: #1CB94E;
                outline: none;
                color: #fff;
                font-size: 14px;
                height: auto;
                font-weight: normal;
                padding: 14px 0;
                text-transform: uppercase;
                border-color: #1CB94A;
            }
            .btn-register:hover,
            .btn-register:focus {
                color: #fff;
                background-color: #1CA347;
                border-color: #1CA347;
            }

        </style>
        <script>
            $(function () {

                $('#login-form-link').click(function (e) {
                    $("#login-form").delay(100).fadeIn(100);
                    $("#register-form").fadeOut(100);
                    $('#register-form-link').removeClass('active');
                    $(this).addClass('active');
                    e.preventDefault();
                });
                $('#register-form-link').click(function (e) {
                    $("#register-form").delay(100).fadeIn(100);
                    $("#login-form").fadeOut(100);
                    $('#login-form-link').removeClass('active');
                    $(this).addClass('active');
                    e.preventDefault();
                });

            });
        </script>
    </head>
    <body  style="background-image: url('img/login-background.jpg')">
        <%

            Database db = new Database();
            db.connect();

            try {
                session.setAttribute("username", null);
                session.setAttribute("id", null);

                session.setAttribute("sM", null);
                session.setAttribute("eM", null);

                if (request.getParameter("logout") != null && !request.getParameter("logout").equals("")) {
                    response.sendRedirect("home.jsp");
                }

                Statement st1 = db.connection.createStatement();
                String q1 = "";
                ResultSet rs1;
                Statement st2 = db.connection.createStatement();
                String q2 = "";
                ResultSet rs2;
                Statement st3 = db.connection.createStatement();
                String q3 = "";
                ResultSet rs3;

                String sM = "";
                String eM = "";

                String type = request.getParameter("type");

                if (type != null) {
                    String username = request.getParameter("username");
                    String password = request.getParameter("password");

                    q1 = "select * from user where username='" + username + "'";
                    rs1 = st1.executeQuery(q1);

                    if (type.equals("login")) {

                        if (rs1.next()) {
                            if (rs1.getString("password").equals(password)) {
                                session.setAttribute("username", username);
                                session.setAttribute("id", rs1.getString("id"));

                                session.setAttribute("sM", "Successfully Logged In");
                                response.sendRedirect("home.jsp");
                            } else {
                                session.setAttribute("eM", "Password Missmatch");
                            }
                        } else {
                            session.setAttribute("eM", "Username Doesn't Exists");
                        }

                    } else if (type.equals("register")) {
                        if (rs1.next()) {
                            session.setAttribute("eM", "Username Already Exists!");
                        } else {
                            String pass = request.getParameter("password");
                            String pass2 = request.getParameter("password2");
                            if (pass != null && pass2 != null && pass.equals(pass2)) {
                                q2 = "select max(id)+1 as mxid from user";
                                rs2 = st2.executeQuery(q2);
                                rs2.next();

                                String id = rs2.getString("mxid");
                                String name = request.getParameter("name");
                                String address = request.getParameter("address");
                                String telephone = request.getParameter("telephone");
                                String email = request.getParameter("email");

                                q3 = "insert into user(id,username,password,name,address,telephone,email) values(" 
                                        + id + ",'" + username + "','" + password + "','" + name + "','" + address + "','" + telephone + "','" + email + "')";
                                System.out.println(q3);
                                st3.executeUpdate(q3);
                                session.setAttribute("username", username);
                                session.setAttribute("id", rs2.getString("mxid"));

                                session.setAttribute("sM", "Successfully Registered");

                                response.sendRedirect("home.jsp");
                            } else {
                                session.setAttribute("eM", "Password Missmatch!");
                            }
                        }
                    }
                }
                
                if(request.getParameter("logout")!=null) {
                    session.setAttribute("sM", "Logged Out!");
                }

        %>
        <div class="container">
            <%if (session.getAttribute("sM") != null) {%>
            <div class="alert alert-success">
                <Strong><%=session.getAttribute("sM")%></Strong>
            </div>
            <%}%>
            <%if (session.getAttribute("eM") != null) {%>
            <div class="alert alert-danger">
                <Strong><%=session.getAttribute("eM")%></Strong>
            </div>
            <%}%>
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="panel panel-login">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-6">
                                    <a href="#" class="active" id="login-form-link">Login</a>
                                </div>
                                <div class="col-xs-6">
                                    <a href="#" id="register-form-link">Register</a>
                                </div>
                            </div>
                            <hr>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form id="login-form" action="loginRegister.jsp" method="post" role="form" style="display: block;">
                                        <div class="form-group">
                                            <input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="Username" required="">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password" required="">
                                        </div>
                                        <%--
                                        <div class="form-group text-center">
                                            <input type="checkbox" tabindex="3" class="" name="remember" id="remember">
                                            <label for="remember"> Remember Me</label>
                                        </div>
                                        --%>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-4 col-sm-offset-3">
                                                    <input type="hidden" name="type" value="login"/>
                                                    <input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Log In">
                                                </div>
                                                <div class="col-sm-3">
                                                    <input type="button" onclick="window.location.href='home.jsp'" name="register-submit" id="register-submit" tabindex="4" class="form-control btn btn-register" value="Go Home">
                                                </div>
                                            </div>
                                        </div>
                                        <%--
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="text-center">
                                                        <a href="https://phpoll.com/recover" tabindex="5" class="forgot-password">Forgot Password?</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        --%>
                                    </form>
                                    <form id="register-form" action="loginRegister.jsp" method="post" role="form" style="display: none;">
                                        <div class="form-group">
                                            <input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="Username" value="" required="">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password" required="">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="password2" id="confirm-password" tabindex="2" class="form-control" placeholder="Confirm Password" required="">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="name" id="username" tabindex="1" class="form-control" placeholder="Name" >
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="address" id="username" tabindex="1" class="form-control" placeholder="Address" >
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="telephone" id="username" tabindex="1" class="form-control" placeholder="Telephone">
                                        </div>
                                        <div class="form-group">
                                            <input type="email" name="email" id="email" tabindex="1" class="form-control" placeholder="Email Address" >
                                        </div>
                                        
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-4 col-sm-offset-3">
                                                    <input type="hidden" name="type" value="register"/>
                                                    <input type="submit" name="register-submit" id="register-submit" tabindex="4" class="form-control btn btn-register" value="Register Now">
                                                </div>
                                                <div class="col-sm-3">
                                                    <input type="button" onclick="window.location.href='home.jsp'" name="register-submit" id="register-submit" tabindex="4" class="form-control btn btn-register" value="Go Home">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                db.close();
            }
        %>
    </body>
</html>

