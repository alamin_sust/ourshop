<%-- 
    Document   : products
    Created on : Apr 23, 2019, 3:16:10 AM
    Author     : md_al
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.ourshop.connection.Database"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <link rel="icon" href="img/favicon.png" type="image/png" />
    <title>Eiser ecommerce</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.css" />
    <link rel="stylesheet" href="vendors/linericon/style.css" />
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <link rel="stylesheet" href="css/themify-icons.css" />
    <link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css" />
    <link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css" />
    <link rel="stylesheet" href="vendors/nice-select/css/nice-select.css" />
    <link rel="stylesheet" href="vendors/animate-css/animate.css" />
    <link rel="stylesheet" href="vendors/jquery-ui/jquery-ui.css" />
    <!-- main css -->
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/responsive.css" />
  </head>

  <body>
      <%@ include file="header.jsp" %>
      <%
            
            
            String categoryId = request.getParameter("categoryId");
            String label = "All Products";
            String subQ="";
            if(categoryId!=null) {
            Statement stC = db.connection.createStatement();
                                                            String qC = "select * from category where id="+categoryId;
                                                            ResultSet rsC = stC.executeQuery(qC);
                                                            rsC.next();
                                                            label=rsC.getString("name");
                                 subQ=" AND category_id="+ categoryId;    
            }
            
         %>   
    
         

    <!--================Home Banner Area =================-->
    <section class="banner_area">
      <div class="banner_inner d-flex align-items-center">
        <div class="container">
          <div class="banner_content d-md-flex justify-content-between align-items-center">
            <div class="mb-3 mb-md-0">
              <h2><%=label%></h2>
              <p>Our Quality Products</p>
            </div>
            <%--<div class="page_link">
              <a href="index.html">Home</a>
              <a href="category.html">Shop</a>
              <a href="category.html">Women Fashion</a>
            </div>--%>
          </div>
        </div>
      </div>
    </section>
    <!--================End Home Banner Area =================-->

    <!--================Category Product Area =================-->
    <section class="cat_product_area section_gap">
      <div class="container">
        <div class="row flex-row-reverse">
          <div class="col-lg-12">
            <%--<div class="product_top_bar">
              <div class="left_dorp">
                <select class="sorting">
                  <option value="1">Default sorting</option>
                  <option value="2">Default sorting 01</option>
                  <option value="4">Default sorting 02</option>
                </select>
                <select class="show">
                  <option value="1">Show 12</option>
                  <option value="2">Show 14</option>
                  <option value="4">Show 16</option>
                </select>
              </div>
            </div>--%>
            
            <div class="latest_product_inner">
              <div class="row">
                 <%
                 Statement stP = db.connection.createStatement();
                                                            String qP = "select * from product where id>0 " + subQ;
                                                            ResultSet rsP = stP.executeQuery(qP);
                                                            while(rsP.next()) {
                 
                 %>
                  
                <div class="col-lg-4 col-md-6">
                  <div class="single-product">
                    <div class="product-img">
                      <img onclick="location.href='productDetails.jsp?productId=<%=rsP.getString("id")%>'" 
                     class="img-fluid w-100" src="img/products/<%=rsP.getString("id")%>.jpg" alt="" style="cursor: pointer;" />
                <%if(session.getAttribute("username")!=null && session.getAttribute("username").equals("admin")){%>
                <div class="p_icon">
                <a href="addProduct.jsp?productId=<%=rsP.getString("id")%>&update=true">
                  Update
                </a>
                <a href="addProduct.jsp?deleteProductId=<%=rsP.getString("id")%>">
                  Delete
                </a>
                <%--<a href="#">
                  <i class="ti-heart"></i>
                </a>
                <a href="#">
                  <i class="ti-shopping-cart"></i>
                </a>--%>
              </div>
              <%}%>
                    </div>
                    <div class="product-btm">
                      <a href="#" class="d-block">
                        <h4><%=rsP.getString("name")%></h4>
                      </a>
                      <div class="mt-3">
                        <span class="mr-4">$<%=rsP.getString("price")%></span>
                        <del>$<%=rsP.getString("price")%></del>
                      </div>
                    </div>
                  </div>
                </div>

                <%}%>
              </div>
            </div>
          </div>
            
          <%--  
          <div class="col-lg-3">
            <div class="left_sidebar_area">
              <aside class="left_widgets p_filter_widgets">
                <div class="l_w_title">
                  <h3>Browse Categories</h3>
                </div>
                <div class="widgets_inner">
                  <ul class="list">
                    <li>
                      <a href="#">Frozen Fish</a>
                    </li>
                    <li>
                      <a href="#">Dried Fish</a>
                    </li>
                    <li>
                      <a href="#">Fresh Fish</a>
                    </li>
                    <li>
                      <a href="#">Meat Alternatives</a>
                    </li>
                    <li>
                      <a href="#">Fresh Fish</a>
                    </li>
                    <li>
                      <a href="#">Meat Alternatives</a>
                    </li>
                    <li>
                      <a href="#">Meat</a>
                    </li>
                  </ul>
                </div>
              </aside>

              <aside class="left_widgets p_filter_widgets">
                <div class="l_w_title">
                  <h3>Product Brand</h3>
                </div>
                <div class="widgets_inner">
                  <ul class="list">
                    <li>
                      <a href="#">Apple</a>
                    </li>
                    <li>
                      <a href="#">Asus</a>
                    </li>
                    <li class="active">
                      <a href="#">Gionee</a>
                    </li>
                    <li>
                      <a href="#">Micromax</a>
                    </li>
                    <li>
                      <a href="#">Samsung</a>
                    </li>
                  </ul>
                </div>
              </aside>

              <aside class="left_widgets p_filter_widgets">
                <div class="l_w_title">
                  <h3>Color Filter</h3>
                </div>
                <div class="widgets_inner">
                  <ul class="list">
                    <li>
                      <a href="#">Black</a>
                    </li>
                    <li>
                      <a href="#">Black Leather</a>
                    </li>
                    <li class="active">
                      <a href="#">Black with red</a>
                    </li>
                    <li>
                      <a href="#">Gold</a>
                    </li>
                    <li>
                      <a href="#">Spacegrey</a>
                    </li>
                  </ul>
                </div>
              </aside>

              <aside class="left_widgets p_filter_widgets">
                <div class="l_w_title">
                  <h3>Price Filter</h3>
                </div>
                <div class="widgets_inner">
                  <div class="range_item">
                    <div id="slider-range"></div>
                    <div class="">
                      <label for="amount">Price : </label>
                      <input type="text" id="amount" readonly />
                    </div>
                  </div>
                </div>
              </aside>
            </div>
          </div>--%>
        </div>
      </div>
    </section>
    <!--================End Category Product Area =================-->

    <%@ include file="footer.jsp" %>
  </body>
</html>
