-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: ourshop
-- ------------------------------------------------------
-- Server version	5.7.25-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(1010) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Zara'),(2,'H & M'),(3,'Others');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(1010) DEFAULT NULL,
  `details` varchar(4000) DEFAULT NULL,
  `price` varchar(110) DEFAULT NULL,
  `stock_qty` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (0,NULL,NULL,NULL,NULL,NULL),(1,'Style-Ocean sweater','Fab-100% Cotton interlock,230GSM','3.20',12,1),(2,'Style-Ocean sweater Blue','Fab-100% Cotton interlock, 230GSM','3.20',10,1),(4,'Style-Ocean sweater Green','Fab-100% Cotton interlock, 230GSM','3.20',15,1),(5,'Style-Ocean sweater Deep Blue','Fab-100% Cotton interlock,230GSM','3.20',14,1),(6,'Style-Ocean sweater Violet','Fab-100% Cotton interlock, 230GSM','3.20',15,1),(7,'Style-Ocean sweater yellow','Fab-100% Cotton interlock, 230GSM	','3.20',20,1),(8,'Style-Ocean sweater Blue','Fab-100% Cotton interlock, 230GSM','3.20',15,1),(9,'Style-Ocean sweater Violet','Fab-100% Cotton interlock, 230GSM','3.20',20,1),(10,'Style-Mia fancy','Fab-95/5 Ctn/elas,GSM-160','1.80',20,1),(11,'Style-Sasha Tee','Fab-10% ctn GSM-140','1.57',100,1),(12,'Style-Sasha Tee','Fab-10% ctn GSM-140','1.57',100,1),(14,'Style-Edith Hood dress','Fab-100% BCI Cotton S/J 180 GSM','3.60',100,1),(15,'Style-Basenji Tank top','Fab-100% Cotton 120GSm','1.45',100,1),(16,'Style-Edith Hood dress','Fab-100% BCI Cotton S/J 180 GSM','3.50',100,1),(17,'Style-Edith Hood dress','Fab-100% BCI Cotton S/J 180 GSM','3.50',100,1),(18,'Style-Edith Hood dress','Fab-100% BCI Cotton S/J 180 GSM','3.50',100,1),(19,'Style-Melissa tee','Fab-95% Cotton 5% Elastane S/J 160 GSM','2.00',100,1),(20,'Style-Lic. Palma tee','Fab-100% BCI Cotton S/J 150 GSM','1.70',100,1),(21,'Style-Edith Hood dress','Fab-100% Cotton 180 GSm','3.50',100,1),(22,'Style-Twinkle tee','Fab-98% Cotton 2% Viscose  S/J 150GSM','2.43',100,1),(23,'Style-Twinkle tee','Fab-100% Cotton S/J 150GSMConsumption : 4Y 10â??         ','2.58',100,1),(24,'Style-Basenji Tank top','Fab-100% BCI cotton 120 GSM','1.50',100,1),(25,'Style-Basenji Tank top','Fab-100% BCI cotton 120 GSM','1.50',100,1),(26,'Sty-Lic. Magna dress','Fab-60% BCI Cotton 40% Polyester S/J 140 GSM','3.80',100,1),(27,'Sty-Lic. Magna dress','Fab-60% BCI Cotton 40% Polyester S/J 140 GSM','3.80',100,1),(29,'Sty-Lic. Magna dress','Fab-60% BCI Cotton 40% Polyester S/J 140 GSM','3.80',100,1),(30,'Sty-Lic. Magna dress','Fab-60% BCI Cotton 40% Polyester S/J 140 GSM','3.80',100,1),(31,'Sty-Lic. Magna dress','Fab-60% BCI Cotton 40% Polyester S/J 140 GSM','3.80',100,1),(32,'Sty-Lic. Magna dress','Fab-60% BCI Cotton 40% Polyester S/J 140 GSM','3.80',100,1),(33,'Sty-Lic. Magna dress','Fab-60% BCI Cotton 40% Polyester S/J 140 GSM','3.80',100,1),(34,'Sty-Lic. Magna dress','Fab-60% BCI Cotton 40% Polyester S/J 140 GSM','3.80',100,1),(35,'CnA-mens T-Shirt','Fabrication:100% Cotton,S/J Slub;160GSM','4.45',100,3),(36,'CnA-mens T-Shirt','Fabrication: 80% Cotton,20% Polyester;S/J,160 GSM','5.90',100,3),(37,'CnA-mens T-Shirt','Fabrication: 99% cotton,1% viscose, S/J, 160 GSM','4.90',100,3),(38,'CnA-mens T-Shirt','Fabrication:100% Cotton,S/J,160 GSM','5.15',100,3),(39,'CnA-mens T-Shirt','Fabrication:100% cotton,S/J,160GSM','5.00',100,3),(40,'CnA-mens T-Shirt','Fabrication:100% cotton,S/J,160GSM','5.00',100,3),(41,'CnA-mens T-Shirt','Fabrication:100% cotton,S/J,160GSM','3.60',100,3),(42,'CnA-mens T-Shirt','Fabrication:  60% Cotton,40% Polyester;S/J,160GSM','5.60',100,3),(43,'CnA-mens T-Shirt','Fabrication: 50% Polyester,38% Cotton,12% Viscose,S/J,160GSM','5.15',100,3),(44,'CnA-mens T-Shirt','Fabrication: 100% Cotton, Y/D S/J,220 GSM ','8.90',100,3),(45,'CnA-mens T-Shirt','Fabrication:100% cotton,Honeycomb pique,220GSM','6.50',100,3),(46,'CnA-mens t-shirt','Fabrication: 60% cotton,40% Polyester;S/J,160 GSM','4.45',100,3),(47,'CnA-mens t-shirt','Fabrication: 100% Cotton,S/J,160','5.30',100,3),(48,'CnA-mens t-shirt','Fabrication: 82% Cotton,18% Polyester;S/J,160','5.50',100,3),(49,'CnA-mens t-shirt','Fabrication:72% Cotton,28% Polyester;S/J,180 GSM','6.10',100,3),(50,'CnA-mens t-shirt','Fabrication: 100% Cotton,S/J,180 GSM','6.50',100,3),(51,'CnA Mens-Winter','Fabrication: 100% polyester; Sherpa fleece,240GSM','16.10',100,3),(52,'','','',0,1),(53,'CnA Mens-Winter','Fabrication:100% Polyester;Microfleece;260 GSM','7.85',100,3),(54,'CnA-mens Winter','Fabrication:100% Cotton Melange;Brushed Fleece;280','8.70',100,3),(55,'CnA-mens Winter','Fabrication: 60% Cotton,40% Polyester;330GSM','7.10',100,3),(56,'CnA-mens Winter','Fabrication:100% polyester;Sherpa fleece;270GSM','9.00',100,3),(57,'CnA-mens Winter','Fabrication:100% Polyester;Camou Sweater fleece;360GSM','13.15',100,3),(58,'CnA-mens Winter','Fabrication:100% Polyester; Bonded Fleece;480GSM','13.30',100,3),(59,'CnA-mens Winter','Fabrication:100% Polyester; Bonded Fleece;480GSM','13.30',100,3),(60,'CnA-mens Winter','Fabrication:100% Polyester; Bonded Fleece;480GSM','13.30',100,3),(61,'CnA-mens Winter','Fabrication:100% Polyester; Bonded Fleece;480GSM','13.30',100,3),(62,'CnA-mens Winter','Fabrication:100% Cotton Melange;Brushed Fleece;280','8.70',100,3),(63,'CnA-mens Winter','Fabrication:100% Cotton Melange;Brushed Fleece;280','8.70',100,3),(64,'CnA-mens Winter','Fabrication:100% Polyester;Microfleece;260 GSM','7.85',100,3),(65,'CnA-mens Winter','Fabrication:100% Polyester;Microfleece;260 GSM','7.85',100,3),(66,'H&M Boys','','2.25',100,2),(67,'H&M Boys','','2.45',100,2),(68,'H&M Boys','','2.90',100,2),(69,'H&M Boys','','2.85',100,2),(70,'H&M Boys','','2.80',100,2),(71,'H&M Boys','','2.70',100,2),(72,'H&M Boys','','2.95',100,2),(73,'H&M Boys','','3.00',100,2),(74,'H&M Boys','','2.70',100,2),(75,'H&M Boys','','2.50',100,2),(76,'H&M Boys','','2.90',100,2),(77,'H&M Boys','','3.00',100,2),(78,'H&M Boys','','3.00',100,1),(79,'H&M Boys','','3.00',100,2),(80,'H&M Boys','','3.00',100,2),(81,'G star t shirt','','6.53',100,3),(82,'G Star T-Shirt','','2.91',100,3),(83,'G Star T-Shirt','Flock printed','5.00',100,3),(84,'G Star T-Shirt','flock print\r\n','5.00',100,3),(85,'G Star T-Shirt','flock print','5.00',100,3),(86,'G Star T-Shirt','flock print','5.00',100,3),(87,'G Star T-Shirt','flock print','5.00',100,3),(88,'G Star T-Shirt','flock print','5.00',100,3),(89,'G Star T-Shirt','flock contains','5.1',100,3),(90,'G Star T-Shirt','Foil printed','5.2',100,3),(91,'G Star T-Shirt','','9.15',100,3),(92,'G Star T-Shirt','','7.50',100,3),(93,'G Star T-Shirt','','7.96',100,3),(94,'G Star T-Shirt','','7.96',100,3),(95,'G Star T-Shirt MEN','','6.70',100,3),(96,'G Star T-Shirt MEN','','6.70',100,3),(97,'G Star T-Shirt MEN','','6.70',100,3),(98,'G Star T-Shirt MEN','','6.70',100,3),(99,'G Star MEN','','8.4',100,3),(100,'G Star MEN','','8.4',100,3),(101,'G Star MEN','','8.4',100,3),(102,'G Star MEN','','8.4',100,3),(103,'G Star MEN','','8.4',100,3),(104,'G Star MEN','','6.70',100,3),(105,'G Star MEN','','4.5',100,3),(106,'G Star MEN','','4.5',100,3),(107,'G Star MEN','','4.5',100,3),(108,'G Star MEN','','6.00',100,3),(109,'G Star MEN','','6.00',100,3),(110,'G Star MEN','','6.00',100,3),(111,'G Star MEN','','6.50',100,3),(112,'G Star MEN','','6.50',100,3),(113,'G Star MEN','','6.50',100,3),(114,'G Star MEN','','7.50',100,3),(115,'H&M Shirts','Short-sleeved shirt in woven cotton fabric with a turn-down collar...Â COMPOSITIONÂ Cotton 100%','7.25',100,2),(116,'H&M Shirts','Shirt in woven cotton fabric with a button-down collar. Â COMPOSITIONÂ Cotton 100%','10.90',100,2),(117,'H&M Shirts','Shirt in chambray woven cotton fabric with a button-down collar, classic button placket, and open chest pocket. COMPOSITIONcotton 100%','9.60',100,2),(118,'H&M Shirts','Checked shirt in woven cotton fabric with a button-down collar. COMPOSITIONcotton 100%','10.29',100,2),(119,'H&M Shirts','Checked shirt in oxford cotton with a button-down collar. Â COMPOSITIONÂ Cotton 100%','11.81',100,2),(120,'H&M Shirts','Checked shirt in soft cotton flannel with a button-down collar. Â COMPOSITIONÂ Cotton 100%','10.48',100,2),(121,'H&M Shirts','Shirt in woven cotton fabric with a turn-down collarÂ COMPOSITIONÂ Cotton 100%','11.30',100,2),(122,'H&M Shirts','Checked shirt in woven cotton fabric with a turn-down collar, buttons without placket, and yoke at back. Â COMPOSITIONÂ Cotton 100%','11.62',100,2),(123,'H&M Shirts','Shirt in a woven TencelÂ® lyocell and cotton blend. Â COMPOSITIONÂ Cotton 52%, Lyocell 48%','12.40',100,2),(124,'H&M Shirts','Straight-cut shirt in washed denim with a collar. COMPOSITIONCotton 100%','11.05',100,2),(125,'H&M Shirts','Shirt in cotton denim with a cutaway collar. COMPOSITIONcotton 100%','11.41',100,2),(126,'H&M Shirts','Shirt in an airy linen and cotton blend. Â COMPOSITIONÂ Cotton 70% Linen 30% ','11.40',100,2),(127,'H&M Shirts','Straight-cut, gently tapered shirt in oxford cotton. Â COMPOSITIONÂ Cotton 100%','11.25',100,2),(128,'H&M Shirts','Shirt in oxford cotton with a button-down collar, classic button placket, and open chest pocket. Â COMPOSITIONÂ Cotton 100%, ','9.95',100,2),(129,'H&M Shirts','Plaid shirt in a soft viscose and cotton blend. COMPOSITIONÂ Cotton 56%, Viscose 44%','11.65',100,2),(130,'H&M Shirts','Plaid shirt in a soft viscose and cotton blend. COMPOSITIONÂ Cotton 56%, Viscose 44%','11.65',100,2),(131,'H&M Shirts','Plaid shirt in a soft viscose and cotton blend. COMPOSITIONÂ Cotton 56%, Viscose 44%','11.65',100,2),(132,'H&M Shirts','Straight-cut, gently tapered shirt in lightweight cotton flannelÂ COMPOSITIONÂ Cotton 100%','10.86',100,2),(133,'H&M Shirts','PREMIUM QUALITY. Shirt in soft, lightweight flannel made from premium cottonÂ COMPOSITIONÂ Cotton 100%, ','12.60',100,2),(134,'H&M Shirts','Straight-cut, relaxed-fit shirt in airy, woven cotton fabric with a band collar. Â COMPOSITIONÂ Cotton 100%','11.22',100,2),(135,'H&M Shirts','Straight-cut, gently tapered shirt in denim. COMPOSITIONÂ Linen 52%, Cotton 48%','10.07',100,2),(136,'H&M Shirts','Straight-cut, gently tapered shirt in denim. COMPOSITIONÂ Linen 52%, Cotton 48%','10.07',100,2),(137,'H&M Shirts','Straight-cut, gently tapered shirt in denim. COMPOSITIONÂ Linen 52%, Cotton 48%','10.07',100,2),(138,'H&M Shirts','CONSCIOUS. Shirt in organic cotton oxford with a button-down collar. COMPOSITIONcotton 100%','11.43',100,2),(139,'H&M Shirts','Short-sleeved shirt in soft, woven fabric made from a viscose and cotton blend with a printed pattern. Â COMPOSITIONÂ Cotton 61%, Viscose 39%','11.05',100,2),(140,'H&M Shirts','Short-sleeved shirt in a woven cotton blend with a printed pattern. COMPOSITIONÂ Viscose 57%, Polyester 43%','9.94',100,2),(141,'H&M Shirts','Striped shirt in airy oxford cotton fabric.Â COMPOSITIONÂ Cotton 100%','10.30',100,2),(142,'H&M Shirts','Shirt in woven cotton twill. Â COMPOSITIONÂ Cotton 100%, ','9.82',100,2),(143,'H&M Shirts','Shirt in airy, woven viscose fabric with a turn-down collar.COMPOSITIONViscose 100%','8.77',100,2),(144,'H&M Shirts','Shirt in airy, woven viscose fabric with a turn-down collar.COMPOSITIONViscose 100%','8.77',100,2),(145,'H&M Shirts','Shirt in airy, woven viscose fabric with a turn-down collar.COMPOSITIONViscose 100%','8.77',100,2),(146,'H&M Shirts','Straight-cut shirt in woven viscose fabric..Â COMPOSITIONÂ Viscose 100%','13.52',100,2),(147,'Nightwear for Women','Nightwear for Women','4.50',100,2),(148,'Nightwear for Women','Nightwear for Women','3.80',100,2),(149,'Nightwear for Women','Nightwear for Women','3.80',100,2),(150,'Nightwear for Women','Nightwear for Women','4.80',100,2),(151,'Nightwear for Women','Nightwear for Women','3.40',100,2),(152,'Nightwear for Women','Nightwear for Women','3.40',100,2),(153,'Nightwear for Women','Nightwear for Women','3.40',100,2),(154,'Nightwear for Women','Nightwear for Women','4.25',100,2),(155,'Nightwear for Women','Nightwear for Women','4.25',100,2),(156,'Nightwear for Women','Nightwear for Women','4.25',100,2),(157,'Nightwear for Women','Nightwear for Women','4.25',100,2),(158,'Nightwear for Women','Nightwear for Women','4.25',100,2),(159,'Nightwear for Women','Nightwear for Women','4.25',100,2),(160,'Nightwear for Women','Nightwear for Women','5.05',100,2),(161,'Nightwear for Women','Nightwear for Women','5.05',100,2),(162,'Nightwear for Women','Nightwear for Women','5.05',100,2),(163,'Nightwear for Women','Nightwear for Women','5.05',100,2),(164,'Nightwear for Women','Nightwear for Women','5.05',100,2),(165,'Nightwear for Women','Nightwear for Women','5.05',100,2),(166,'Nightwear for Women','Nightwear for Women','5.05',100,2),(167,'Nightwear for Women','Nightwear for Women','4.60',100,2),(168,'Nightwear for Women','Nightwear for Women','4.60',100,2),(169,'Nightwear for Women','Nightwear for Women','4.60',100,2),(170,'Nightwear for Women','Nightwear for Women','4.60',100,2),(171,'Nightwear for Women','Nightwear for Women','4.60',100,2),(172,'Nightwear for Women','Nightwear for Women','4.60',100,2),(173,'Nightwear for Women','Nightwear for Women','4.60',100,2),(174,'Nightwear for Women','Nightwear for Women','4.60',100,2),(175,'Nightwear for Women','Nightwear for Women','3.00',100,2),(176,'Nightwear for Women','Nightwear for Women','2.85',100,2),(177,'Nightwear for Women','Nightwear for Women','2.85',100,2),(178,'Nightwear for Women','Nightwear for Women','3.80',100,2),(179,'Nightwear for Women','Nightwear for Women','3.80',100,2),(180,'Nightwear for Women','Nightwear for Women','3.80',100,2),(181,'Nightwear for Women','Nightwear for Women','3.80',100,2),(182,'Nightwear for Women','Nightwear for Women','3.80',100,2),(183,'Nightwear for Women','Nightwear for Women','3.35',100,2),(184,'Nightwear for Women','Nightwear for Women','3.35',100,2),(185,'Nightwear for Women','Nightwear for Women','3.35',100,2),(186,'Nightwear for Women','Nightwear for Women','3.35',100,2),(187,'Nightwear for Women','Nightwear for Women','3.35',100,2),(188,'Nightwear for Women','Nightwear for Women','3.35',100,2),(189,'Nightwear for Women','Nightwear for Women','3.35',100,2),(190,'Nightwear for Women','Nightwear for Women','3.80',100,2),(191,'Nightwear for Women','Nightwear for Women','3.80',100,2),(192,'Nightwear for Women','Nightwear for Women','3.80',100,2),(193,'Nightwear for Women','Nightwear for Women','4.95',100,2),(194,'Nightwear for Women','Nightwear for Women','4.95',100,2),(195,'Nightwear for Women','Nightwear for Women','4.95',100,2),(196,'Nightwear for Women','Nightwear for Women','4.95',100,2),(198,'Nightwear for Women','Nightwear for Women','4.95',100,2),(199,'Nightwear for Women','Nightwear for Women','5.05',100,2),(200,'Nightwear for Women','Nightwear for Women','5.05',100,2),(201,'Nightwear for Women','Nightwear for Women','5.05',100,2),(202,'Nightwear for Women','Nightwear for Women','4.80',100,2),(203,'Nightwear for Women','Nightwear for Women','5.25',100,2),(204,'Nightwear for Women','Nightwear for Women','5.25',100,2),(205,'Nightwear for Women','Nightwear for Women','5.25',100,2),(206,'Nightwear for Women','Nightwear for Women','5.25',100,2),(207,'Nightwear for Women','Nightwear for Women','5.00',100,2),(208,'Short Pant','chambray, pocket print','8.00',100,2),(209,'Short Pant','chambray, pocket print','8.00',100,2),(210,'Short Pant','Short Pant','9.00',100,2),(211,'Short Pant','Short Pant','7.5 - 8.5',100,2),(212,'Short Pant','Short Pant','8.00',100,2),(213,'Short Pant','Short Pant','6 - 7',100,2),(214,'Short Pant','Short Pant','7.50',100,2),(215,'Short Pant','Short Pant','7.5',100,2),(216,'Short Pant','Short Pant','5 - 6',100,2),(217,'Short Pant','Short Pant','10.00',100,2),(218,'Short Pant','Short Pant','10.00',100,2),(219,'Short Pant','Short Pant','7.50',100,2),(220,'Short Pant','Short Pant','9.00',100,2),(221,'Short Pant','Short Pant','7.50',100,2),(222,'Short Pant','Short Pant','8.25',100,2),(223,'Short Pant','Short Pant','6.50',100,2),(224,'Short Pant','Short Pant','7.75',100,2),(225,'Short Pant','Short Pant','7.00',100,2),(226,'Short Pant','Short Pant','11 - 12.50',100,2),(227,'Short Pant','Short Pant','10 - 12',100,2),(228,'Short Pant','Short Pant','13 - 14',100,2),(229,'Short Pant','Short Pant','6.50',100,2),(230,'Short Pant','Short Pant with Drawing','6.50',100,2),(231,'Short Pant','Short Pant ','5.50 - 6.50',100,2),(232,'Short Pant','Short Pant','7 - 8',100,2),(233,'Short Pant','Short Pant','7.50',100,2);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(110) NOT NULL,
  `password` varchar(110) DEFAULT NULL,
  `name` varchar(110) DEFAULT NULL,
  `address` varchar(1010) DEFAULT NULL,
  `telephone` varchar(110) DEFAULT NULL,
  `email` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (0,'0',NULL,NULL,NULL,NULL,NULL),(1,'admin','1234','','','','');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-23  9:20:36
