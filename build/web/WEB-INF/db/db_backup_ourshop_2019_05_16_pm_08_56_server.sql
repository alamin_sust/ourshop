-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: ourshop
-- ------------------------------------------------------
-- Server version	5.7.25-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(1010) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Zara'),(2,'H & M'),(3,'Others');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(1010) DEFAULT NULL,
  `details` varchar(4000) DEFAULT NULL,
  `price` varchar(110) DEFAULT NULL,
  `stock_qty` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (0,NULL,NULL,NULL,NULL,NULL),(1,'Style-Ocean sweater','Fab-100% Cotton interlock,230GSM','3.20',12,1),(2,'Style-Ocean sweater Blue','Fab-100% Cotton interlock, 230GSM','3.20',10,1),(4,'Style-Ocean sweater Green','Fab-100% Cotton interlock, 230GSM','3.20',15,1),(5,'Style-Ocean sweater Deep Blue','Fab-100% Cotton interlock,230GSM','3.20',14,1),(6,'Style-Ocean sweater Violet','Fab-100% Cotton interlock, 230GSM','3.20',15,1),(7,'Style-Ocean sweater yellow','Fab-100% Cotton interlock, 230GSM	','3.20',20,1),(8,'Style-Ocean sweater Blue','Fab-100% Cotton interlock, 230GSM','3.20',15,1),(9,'Style-Ocean sweater Violet','Fab-100% Cotton interlock, 230GSM','3.20',20,1),(10,'Style-Mia fancy','Fab-95/5 Ctn/elas,GSM-160','1.80',20,1),(11,'Style-Sasha Tee','Fab-10% ctn GSM-140','1.57',100,1),(12,'Style-Sasha Tee','Fab-10% ctn GSM-140','1.57',100,1),(14,'Style-Edith Hood dress','Fab-100% BCI Cotton S/J 180 GSM','3.60',100,1),(15,'Style-Basenji Tank top','Fab-100% Cotton 120GSm','1.45',100,1),(16,'Style-Edith Hood dress','Fab-100% BCI Cotton S/J 180 GSM','3.50',100,1),(17,'Style-Edith Hood dress','Fab-100% BCI Cotton S/J 180 GSM','3.50',100,1),(18,'Style-Edith Hood dress','Fab-100% BCI Cotton S/J 180 GSM','3.50',100,1),(19,'Style-Melissa tee','Fab-95% Cotton 5% Elastane S/J 160 GSM','2.00',100,1),(20,'Style-Lic. Palma tee','Fab-100% BCI Cotton S/J 150 GSM','1.70',100,1),(21,'Style-Edith Hood dress','Fab-100% Cotton 180 GSm','3.50',100,1),(22,'Style-Twinkle tee','Fab-98% Cotton 2% Viscose  S/J 150GSM','2.43',100,1),(23,'Style-Twinkle tee','Fab-100% Cotton S/J 150GSMConsumption : 4Y 10â??         ','2.58',100,1),(24,'Style-Basenji Tank top','Fab-100% BCI cotton 120 GSM','1.50',100,1),(25,'Style-Basenji Tank top','Fab-100% BCI cotton 120 GSM','1.50',100,1),(26,'Sty-Lic. Magna dress','Fab-60% BCI Cotton 40% Polyester S/J 140 GSM','3.80',100,1),(27,'Sty-Lic. Magna dress','Fab-60% BCI Cotton 40% Polyester S/J 140 GSM','3.80',100,1),(29,'Sty-Lic. Magna dress','Fab-60% BCI Cotton 40% Polyester S/J 140 GSM','3.80',100,1),(30,'Sty-Lic. Magna dress','Fab-60% BCI Cotton 40% Polyester S/J 140 GSM','3.80',100,1),(31,'Sty-Lic. Magna dress','Fab-60% BCI Cotton 40% Polyester S/J 140 GSM','3.80',100,1),(32,'Sty-Lic. Magna dress','Fab-60% BCI Cotton 40% Polyester S/J 140 GSM','3.80',100,1),(33,'Sty-Lic. Magna dress','Fab-60% BCI Cotton 40% Polyester S/J 140 GSM','3.80',100,1),(34,'Sty-Lic. Magna dress','Fab-60% BCI Cotton 40% Polyester S/J 140 GSM','3.80',100,1),(35,'CnA-mens T-Shirt','Fabrication:100% Cotton,S/J Slub;160GSM','4.45',100,3),(36,'CnA-mens T-Shirt','Fabrication: 80% Cotton,20% Polyester;S/J,160 GSM','5.90',100,3),(37,'CnA-mens T-Shirt','Fabrication: 99% cotton,1% viscose, S/J, 160 GSM','4.90',100,3),(38,'CnA-mens T-Shirt','Fabrication:100% Cotton,S/J,160 GSM','5.15',100,3),(39,'CnA-mens T-Shirt','Fabrication:100% cotton,S/J,160GSM','5.00',100,3),(40,'CnA-mens T-Shirt','Fabrication:100% cotton,S/J,160GSM','5.00',100,3),(41,'CnA-mens T-Shirt','Fabrication:100% cotton,S/J,160GSM','3.60',100,3),(42,'CnA-mens T-Shirt','Fabrication:  60% Cotton,40% Polyester;S/J,160GSM','5.60',100,3),(43,'CnA-mens T-Shirt','Fabrication: 50% Polyester,38% Cotton,12% Viscose,S/J,160GSM','5.15',100,3),(44,'CnA-mens T-Shirt','Fabrication: 100% Cotton, Y/D S/J,220 GSM ','8.90',100,3),(45,'CnA-mens T-Shirt','Fabrication:100% cotton,Honeycomb pique,220GSM','6.50',100,3),(46,'CnA-mens t-shirt','Fabrication: 60% cotton,40% Polyester;S/J,160 GSM','4.45',100,3),(47,'CnA-mens t-shirt','Fabrication: 100% Cotton,S/J,160','5.30',100,3),(48,'CnA-mens t-shirt','Fabrication: 82% Cotton,18% Polyester;S/J,160','5.50',100,3),(49,'CnA-mens t-shirt','Fabrication:72% Cotton,28% Polyester;S/J,180 GSM','6.10',100,3),(50,'CnA-mens t-shirt','Fabrication: 100% Cotton,S/J,180 GSM','6.50',100,3),(51,'CnA Mens-Winter','Fabrication: 100% polyester; Sherpa fleece,240GSM','16.10',100,3),(52,'','','',0,1),(53,'CnA Mens-Winter','Fabrication:100% Polyester;Microfleece;260 GSM','7.85',100,3),(54,'CnA-mens Winter','Fabrication:100% Cotton Melange;Brushed Fleece;280','8.70',100,3),(55,'CnA-mens Winter','Fabrication: 60% Cotton,40% Polyester;330GSM','7.10',100,3),(56,'CnA-mens Winter','Fabrication:100% polyester;Sherpa fleece;270GSM','9.00',100,3),(57,'CnA-mens Winter','Fabrication:100% Polyester;Camou Sweater fleece;360GSM','13.15',100,3),(58,'CnA-mens Winter','Fabrication:100% Polyester; Bonded Fleece;480GSM','13.30',100,3),(59,'CnA-mens Winter','Fabrication:100% Polyester; Bonded Fleece;480GSM','13.30',100,3),(60,'CnA-mens Winter','Fabrication:100% Polyester; Bonded Fleece;480GSM','13.30',100,3),(61,'CnA-mens Winter','Fabrication:100% Polyester; Bonded Fleece;480GSM','13.30',100,3),(62,'CnA-mens Winter','Fabrication:100% Cotton Melange;Brushed Fleece;280','8.70',100,3),(63,'CnA-mens Winter','Fabrication:100% Cotton Melange;Brushed Fleece;280','8.70',100,3),(64,'CnA-mens Winter','Fabrication:100% Polyester;Microfleece;260 GSM','7.85',100,3),(65,'CnA-mens Winter','Fabrication:100% Polyester;Microfleece;260 GSM','7.85',100,3),(66,'H&M Boys','','2.25',100,2),(67,'H&M Boys','','2.45',100,2),(68,'H&M Boys','','2.90',100,2),(69,'H&M Boys','','2.85',100,2),(70,'H&M Boys','','2.80',100,2),(71,'H&M Boys','','2.70',100,2),(72,'H&M Boys','','2.95',100,2),(73,'H&M Boys','','3.00',100,2),(74,'H&M Boys','','2.70',100,2),(75,'H&M Boys','','2.50',100,2),(76,'H&M Boys','','2.90',100,2),(77,'H&M Boys','','3.00',100,2),(78,'H&M Boys','','3.00',100,1),(79,'H&M Boys','','3.00',100,2),(80,'H&M Boys','','3.00',100,2);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(110) NOT NULL,
  `password` varchar(110) DEFAULT NULL,
  `name` varchar(110) DEFAULT NULL,
  `address` varchar(1010) DEFAULT NULL,
  `telephone` varchar(110) DEFAULT NULL,
  `email` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (0,'0',NULL,NULL,NULL,NULL,NULL),(1,'admin','1234','','','','');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-16  9:05:11
