<%-- 
    Document   : addProduct
    Created on : Apr 23, 2019, 2:12:48 AM
    Author     : md_al
--%>

<%@page import="java.io.FileOutputStream"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="java.io.File"%>
<%@page import="java.io.DataInputStream"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.ourshop.connection.Database"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8" />
        <meta
            name="viewport"
            content="width=device-width, initial-scale=1, shrink-to-fit=no"
            />
        <link rel="icon" href="img/favicon.png" type="image/png" />
        <title>Eiser ecommerce</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.css" />
        <link rel="stylesheet" href="vendors/linericon/style.css" />
        <link rel="stylesheet" href="css/font-awesome.min.css" />
        <link rel="stylesheet" href="css/themify-icons.css" />
        <link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css" />
        <link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css" />
        <link rel="stylesheet" href="vendors/nice-select/css/nice-select.css" />
        <link rel="stylesheet" href="vendors/animate-css/animate.css" />
        <link rel="stylesheet" href="vendors/jquery-ui/jquery-ui.css" />
        <!-- main css -->
        <link rel="stylesheet" href="css/style.css" />
        <link rel="stylesheet" href="css/responsive.css" />
    </head>

    <body>
        <%@ include file="header.jsp" %>
        <%

            if (session.getAttribute("username") == null || !session.getAttribute("username").equals("admin")) {
               response.sendRedirect("home.jsp");
           }
           
           String name = request.getParameter("name");
           String details = request.getParameter("details");
           String price = request.getParameter("price");
           String stockQty = request.getParameter("stockQty");
           String categoryId = request.getParameter("categoryId");
           String update = request.getParameter("update")==null?"":request.getParameter("update");
           
           if(request.getParameter("deleteProductId")!=null) {
               Statement st = db.connection.createStatement();
               String q = "delete from product where id="+request.getParameter("deleteProductId");
               st.executeUpdate(q);
               response.sendRedirect("home.jsp");
           }
           
           String nameDb = "";
           String detailsDb = "";
           String priceDb = "";
           String stockQtyDb = "0";
           String categoryIdDb = "";
           
           String productId = request.getParameter("productId");
           if (productId == null) {
                Statement st = db.connection.createStatement();
                String q = "select max(id)+1 as mx from product";
                ResultSet rs = st.executeQuery(q);
                rs.next();
                productId = rs.getString("mx");
            }
           
           Boolean isImageUpload = null;
            if (request.getParameter("isImageUpload") != null && request.getParameter("isImageUpload").equals("true")) {
                isImageUpload = true;
            }
           if (isImageUpload != null && isImageUpload) {
                //file start
                String saveFile = new String();
                String saveFile2 = new String();
                String contentType = request.getContentType();
                if ((contentType != null) && (contentType.indexOf("multipart/form-data") >= 0)) {
                    DataInputStream in = new DataInputStream(request.getInputStream());

                    int formDataLength = request.getContentLength();
                    byte dataBytes[] = new byte[formDataLength];
                    int byteRead = 0;
                    int totalBytesRead = 0;

                    while (totalBytesRead < formDataLength) {
                        byteRead = in.read(dataBytes, totalBytesRead, formDataLength);
                        totalBytesRead += byteRead;
                    }
                    String file = new String(dataBytes);

                    saveFile = file.substring(file.indexOf("filename=\"") + 10);
                    saveFile = saveFile.substring(0, saveFile.indexOf("\n"));
                    saveFile = saveFile.substring(saveFile.lastIndexOf("\\") + 1, saveFile.indexOf("\""));

                    int lastIndex = contentType.lastIndexOf("=");

                    String boundary = contentType.substring(lastIndex + 1, contentType.length());

                    int pos;

                    pos = file.indexOf("filename=\"");
                    pos = file.indexOf("\n", pos) + 1;
                    pos = file.indexOf("\n", pos) + 1;
                    pos = file.indexOf("\n", pos) + 1;

                    int boundaryLocation = file.indexOf(boundary, pos) - 4;

                    int startPos = ((file.substring(0, pos)).getBytes()).length;
                    int endPos = ((file.substring(0, boundaryLocation)).getBytes()).length;

                    saveFile = "C:\\Users\\md_al\\Documents\\NetBeansProjects\\OurShop\\build\\web\\img\\products\\" + productId + ".jpg";
                    saveFile2 = "C:\\Users\\md_al\\Documents\\NetBeansProjects\\OurShop\\web\\img\\products\\" + productId + ".jpg";
                    //saveFile = "C:/uploadDir2/" + saveFile;
                    //out.print(saveFile);
                    File ff = new File(saveFile);
                    File ff2 = new File(saveFile2);

                    try {
                        FileOutputStream fileOut = new FileOutputStream(ff);
                        fileOut.write(dataBytes, startPos, (endPos - startPos));
                        fileOut.flush();
                        fileOut.close();
                        FileOutputStream fileOut2 = new FileOutputStream(ff2);
                        fileOut2.write(dataBytes, startPos, (endPos - startPos));
                        fileOut2.flush();
                        fileOut2.close();
                        session.setAttribute("sM", "Uploaded Successfully");
                    } catch (Exception e) {
                        out.println(e);
                    }

                }
            }
           
           
            if (categoryId!=null) {
                if(update.equals("")) {
                Statement st2 = db.connection.createStatement();
                String q2 = "insert into product (id,name,details,price,stock_qty,category_id) values("
                        + productId + ",'" + name + "','" + details + "','" + price + "',"
                        + stockQty + "," + categoryId + ")";
                st2.executeUpdate(q2);
                productId = String.valueOf(Integer.parseInt(productId)+1);
                session.setAttribute("sM", "Successfully Inserted!");
                } else {
                    Statement st2 = db.connection.createStatement();
                String q2 = "update product set name='"+name+"',details='"+details
                        +"',price='"+price+"',stock_qty="+stockQty+",category_id="+categoryId+" where id="+productId;
                st2.executeUpdate(q2);
                session.setAttribute("sM", "Successfully Updated!");
                }
            }
            
            if (!update.equals("")){
                Statement st = db.connection.createStatement();
                String q = "select * from product where id="+productId;
                ResultSet rs = st.executeQuery(q);
                rs.next();
                nameDb = rs.getString("name");
                detailsDb = rs.getString("details");
                priceDb = rs.getString("price");
                stockQtyDb = rs.getString("stock_qty");
                categoryIdDb = rs.getString("category_id");
           }


        %>  

        

        <!--================Home Banner Area =================-->
        <section class="banner_area">
            <div class="banner_inner d-flex align-items-center">
                <div class="container">
                    <div
                        class="banner_content d-md-flex justify-content-between align-items-center"
                        >
                        <div class="mb-3 mb-md-0">
                            <h2>Add Product</h2>
                            <p>Add new items in the shop</p>
                        </div>
                        <div class="page_link">
                            <a href="home.jsp">Home</a>
                            <a href="addProduct.jsp">Add Product</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--================End Home Banner Area =================-->

        
        
        <!--================Checkout Area =================-->
        <section class="checkout_area section_gap">
            <div class="container">
                
                <%if (session.getAttribute("sM") != null) {%>
        <div class="alert alert-success text-center">
            <%=session.getAttribute("sM")%>
        </div>
        <%session.setAttribute("sM", null);
            }%>
        <%if (session.getAttribute("eM") != null) {%>
        <div class="alert alert-danger text-center">
            <%=session.getAttribute("eM")%>
        </div>
        <%session.setAttribute("eM", null);
            }%>
                
                <div class="billing_details">
                    <div class="row">
                        <div class="col-lg-8">
                            <h3>Product Details</h3>
                            <div class="col-md-12 form-group">
                            <form id="product-image" class="contact-form" enctype="multipart/form-data" 
                              action="addProduct.jsp?productId=<%=productId%>&isImageUpload=true&update=<%=update%>" method="post">
                                <label>Image</label> <input type="file" name="file" value="" required="" onchange="submitForm();"/>
                            <%--<button class="site-btn" type="submit">Upload Image</button>--%>
                        </form>
                            </div>
                            
                            <form class="row contact_form" action="addProduct.jsp" method="post" novalidate="novalidate">
                                <div class="col-md-12 form-group">
                                    <input type="text" class="form-control" id="first" value="<%=nameDb%>" name="name" placeholder="Name"/>
                                </div>
                                
                                <div class="col-md-12 form-group">
                                    
                                    <textarea
                                        class="form-control"
                                        name="details"
                                        id="message"
                                        rows="1"
                                        placeholder="Item Description"
                                        value="<%=detailsDb%>"
                                        ></textarea>
                                </div>
                               <div class="col-md-12 form-group p_star">
                                   <select class="country_select" required="" name="categoryId">
                                        <%
                                                            Statement stC = db.connection.createStatement();
                                                            String qC = "select * from category";
                                                            ResultSet rsC = stC.executeQuery(qC);
                                                            while(rsC.next()) {
                                                        %>
                                                        <option <%if(rsC.getString("id").equals(categoryIdDb)){%>selected<%}%> value="<%=rsC.getString("id")%>"><%=rsC.getString("name")%></option>
                                        <%}%>
                                    </select>
                                </div>
                                <div class="col-md-12 form-group">
                                    <input
                                        type="text"
                                        class="form-control"
                                        id="company"
                                        name="price"
                                        value="<%=priceDb%>"
                                        placeholder="Price($)"
                                        />
                                </div>
                                <div class="col-md-12 form-group">
                                    <input
                                        type="number"
                                        class="form-control"
                                        id="number"
                                        value="<%=stockQtyDb%>"
                                        name="stockQty" placeholder="Stock Quantity" value="0" required=""
                                        />
                                </div>
                                    
                                
                                <div class="col-md-12 form-group">
                                    <input type="hidden" name="productId" value="<%=productId%>">
                                    <input type="hidden" name="update" value="<%=update%>">
                                    <button type="submit" value="submit" class="btn submit_btn">
                Insert
              </button>
                                </div>
                                
                            </form>
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>
        <!--================End Checkout Area =================-->

        
        <script>
            function submitForm() {
                document.getElementById("product-image").submit();
            }
        </script>
        
        <%@ include file="footer.jsp" %>
    </body>
</html>
