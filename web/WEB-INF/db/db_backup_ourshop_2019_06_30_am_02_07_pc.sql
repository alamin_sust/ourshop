-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: localhost    Database: ourshop
-- ------------------------------------------------------
-- Server version	5.7.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(1010) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Zara'),(2,'H & M'),(3,'Others'),(4,'C & A'),(5,'G-Star');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(1010) DEFAULT NULL,
  `details` varchar(4000) DEFAULT NULL,
  `price` varchar(110) DEFAULT NULL,
  `stock_qty` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sub_category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (0,NULL,NULL,NULL,NULL,NULL,NULL),(1,'Style-Ocean sweater','Style-Ocean sweater Blue	Fab-100% Cotton interlock, 230GSM','3.20',12,2,5),(2,'Style-Ocean sweater Blue','Fab-100% Cotton interlock, 230GSM','3.20',10,1,NULL),(4,'Style-Ocean sweater Green','Fab-100% Cotton interlock, 230GSM','3.20',15,1,NULL),(5,'Style-Ocean sweater Deep Blue','Fab-100% Cotton interlock,230GSM','3.20',14,1,NULL),(6,'Style-Ocean sweater Violet','Fab-100% Cotton interlock, 230GSM','3.20',15,1,NULL),(7,'Style-Ocean sweater yellow','Fab-100% Cotton interlock, 230GSM	','3.20',20,1,NULL),(8,'Style-Ocean sweater Blue','Fab-100% Cotton interlock, 230GSM','3.20',15,1,NULL),(9,'Style-Ocean sweater Violet','Fab-100% Cotton interlock, 230GSM','3.20',20,1,NULL),(10,'Style-Mia fancy','Fab-95/5 Ctn/elas,GSM-160','1.80',20,1,NULL),(11,'Style-Sasha Tee','Fab-10% ctn GSM-140','1.57',100,1,NULL),(12,'Style-Sasha Tee','Fab-10% ctn GSM-140','1.57',100,1,NULL),(14,'Style-Edith Hood dress','Fab-100% BCI Cotton S/J 180 GSM','3.60',100,1,NULL),(15,'Style-Basenji Tank top','Fab-100% Cotton 120GSm','1.45',100,1,NULL),(16,'Style-Edith Hood dress','Fab-100% BCI Cotton S/J 180 GSM','3.50',100,1,NULL),(17,'Style-Edith Hood dress','Fab-100% BCI Cotton S/J 180 GSM','3.50',100,1,NULL),(18,'Style-Edith Hood dress','Fab-100% BCI Cotton S/J 180 GSM','3.50',100,1,NULL),(19,'Style-Melissa tee','Fab-95% Cotton 5% Elastane S/J 160 GSM','2.00',100,1,NULL),(20,'Style-Lic. Palma tee','Fab-100% BCI Cotton S/J 150 GSM','1.70',100,1,NULL),(21,'Style-Edith Hood dress','Fab-100% Cotton 180 GSm','3.50',100,1,NULL),(22,'Style-Twinkle tee','Fab-98% Cotton 2% Viscose  S/J 150GSM','2.43',100,1,NULL),(23,'Style-Twinkle tee','Fab-100% Cotton S/J 150GSMConsumption : 4Y 10â??         ','2.58',100,1,NULL),(24,'Style-Basenji Tank top','Fab-100% BCI cotton 120 GSM','1.50',100,1,NULL),(25,'Style-Basenji Tank top','Fab-100% BCI cotton 120 GSM','1.50',100,1,NULL),(26,'Sty-Lic. Magna dress','Fab-60% BCI Cotton 40% Polyester S/J 140 GSM','3.80',100,1,NULL),(27,'Sty-Lic. Magna dress','Fab-60% BCI Cotton 40% Polyester S/J 140 GSM','3.80',100,1,NULL),(29,'Sty-Lic. Magna dress','Fab-60% BCI Cotton 40% Polyester S/J 140 GSM','3.80',100,1,NULL),(30,'Sty-Lic. Magna dress','Fab-60% BCI Cotton 40% Polyester S/J 140 GSM','3.80',100,1,NULL),(31,'Sty-Lic. Magna dress','Fab-60% BCI Cotton 40% Polyester S/J 140 GSM','3.80',100,1,NULL),(32,'Sty-Lic. Magna dress','Fab-60% BCI Cotton 40% Polyester S/J 140 GSM','3.80',100,1,NULL),(33,'Sty-Lic. Magna dress','Fab-60% BCI Cotton 40% Polyester S/J 140 GSM','3.80',100,1,NULL),(34,'Sty-Lic. Magna dress','Fab-60% BCI Cotton 40% Polyester S/J 140 GSM','3.80',100,1,NULL),(35,'CnA-mens T-Shirt','Fabrication:100% Cotton,S/J Slub;160GSM','4.45',100,3,NULL),(36,'CnA-mens T-Shirt','Fabrication: 80% Cotton,20% Polyester;S/J,160 GSM','5.90',100,3,NULL),(37,'CnA-mens T-Shirt','Fabrication: 99% cotton,1% viscose, S/J, 160 GSM','4.90',100,3,NULL),(38,'CnA-mens T-Shirt','Fabrication:100% Cotton,S/J,160 GSM','5.15',100,3,NULL),(39,'CnA-mens T-Shirt','Fabrication:100% cotton,S/J,160GSM','5.00',100,3,NULL),(40,'CnA-mens T-Shirt','Fabrication:100% cotton,S/J,160GSM','5.00',100,3,NULL),(41,'CnA-mens T-Shirt','Fabrication:100% cotton,S/J,160GSM','3.60',100,3,NULL),(42,'CnA-mens T-Shirt','Fabrication:  60% Cotton,40% Polyester;S/J,160GSM','5.60',100,3,NULL),(43,'CnA-mens T-Shirt','Fabrication: 50% Polyester,38% Cotton,12% Viscose,S/J,160GSM','5.15',100,3,NULL),(44,'CnA-mens T-Shirt','Fabrication: 100% Cotton, Y/D S/J,220 GSM ','8.90',100,3,NULL),(45,'CnA-mens T-Shirt','Fabrication:100% cotton,Honeycomb pique,220GSM','6.50',100,3,NULL),(46,'CnA-mens t-shirt','Fabrication: 60% cotton,40% Polyester;S/J,160 GSM','4.45',100,3,NULL),(47,'CnA-mens t-shirt','Fabrication: 100% Cotton,S/J,160','5.30',100,3,NULL),(48,'CnA-mens t-shirt','Fabrication: 82% Cotton,18% Polyester;S/J,160','5.50',100,3,NULL),(49,'CnA-mens t-shirt','Fabrication:72% Cotton,28% Polyester;S/J,180 GSM','6.10',100,3,NULL),(50,'CnA-mens t-shirt','Fabrication: 100% Cotton,S/J,180 GSM','6.50',100,3,NULL),(51,'CnA Mens-Winter','Fabrication: 100% polyester; Sherpa fleece,240GSM','16.10',100,3,NULL),(52,'','','',0,1,NULL),(53,'CnA Mens-Winter','Fabrication:100% Polyester;Microfleece;260 GSM','7.85',100,3,NULL),(54,'CnA-mens Winter','Fabrication:100% Cotton Melange;Brushed Fleece;280','8.70',100,3,NULL),(55,'CnA-mens Winter','Fabrication: 60% Cotton,40% Polyester;330GSM','7.10',100,3,NULL),(56,'CnA-mens Winter','Fabrication:100% polyester;Sherpa fleece;270GSM','9.00',100,3,NULL),(57,'CnA-mens Winter','Fabrication:100% Polyester;Camou Sweater fleece;360GSM','13.15',100,3,NULL),(58,'CnA-mens Winter','Fabrication:100% Polyester; Bonded Fleece;480GSM','13.30',100,3,NULL),(59,'CnA-mens Winter','Fabrication:100% Polyester; Bonded Fleece;480GSM','13.30',100,3,NULL),(60,'CnA-mens Winter','Fabrication:100% Polyester; Bonded Fleece;480GSM','13.30',100,3,NULL),(61,'CnA-mens Winter','Fabrication:100% Polyester; Bonded Fleece;480GSM','13.30',100,3,NULL),(62,'CnA-mens Winter','Fabrication:100% Cotton Melange;Brushed Fleece;280','8.70',100,3,NULL),(63,'CnA-mens Winter','Fabrication:100% Cotton Melange;Brushed Fleece;280','8.70',100,3,NULL),(64,'CnA-mens Winter','Fabrication:100% Polyester;Microfleece;260 GSM','7.85',100,3,NULL),(65,'CnA-mens Winter','Fabrication:100% Polyester;Microfleece;260 GSM','7.85',100,3,NULL),(66,'H&M Boys','','2.25',100,2,NULL),(67,'H&M Boys','','2.45',100,2,NULL),(68,'H&M Boys','','2.90',100,2,NULL),(69,'H&M Boys','','2.85',100,2,NULL),(70,'H&M Boys','','2.80',100,2,NULL),(71,'H&M Boys','','2.70',100,2,NULL),(72,'H&M Boys','','2.95',100,2,NULL),(73,'H&M Boys','','3.00',100,2,NULL),(74,'H&M Boys','','2.70',100,2,NULL),(75,'H&M Boys','','2.50',100,2,NULL),(76,'H&M Boys','','2.90',100,2,NULL),(77,'H&M Boys','','3.00',100,2,NULL),(78,'H&M Boys','','3.00',100,1,NULL),(79,'H&M Boys','','3.00',100,2,NULL),(80,'H&M Boys','','3.00',100,2,NULL),(81,'G star t shirt','','6.53',100,3,NULL),(82,'G Star T-Shirt','','2.91',100,3,NULL),(83,'G Star T-Shirt','Flock printed','5.00',100,3,NULL),(84,'G Star T-Shirt','flock print\r\n','5.00',100,3,NULL),(85,'G Star T-Shirt','flock print','5.00',100,3,NULL),(86,'G Star T-Shirt','flock print','5.00',100,3,NULL),(87,'G Star T-Shirt','flock print','5.00',100,3,NULL),(88,'G Star T-Shirt','flock print','5.00',100,3,NULL),(89,'G Star T-Shirt','flock contains','5.1',100,3,NULL),(90,'G Star T-Shirt','Foil printed','5.2',100,3,NULL),(91,'G Star T-Shirt','','9.15',100,3,NULL),(92,'G Star T-Shirt','','7.50',100,3,NULL),(93,'G Star T-Shirt','','7.96',100,3,NULL),(94,'G Star T-Shirt','','7.96',100,3,NULL),(95,'G Star T-Shirt MEN','','6.70',100,3,NULL),(96,'G Star T-Shirt MEN','','6.70',100,3,NULL),(97,'G Star T-Shirt MEN','','6.70',100,3,NULL),(98,'G Star T-Shirt MEN','','6.70',100,3,NULL),(99,'G Star MEN','','8.4',100,3,NULL),(100,'G Star MEN','','8.4',100,3,NULL),(101,'G Star MEN','','8.4',100,3,NULL),(102,'G Star MEN','','8.4',100,3,NULL),(103,'G Star MEN','','8.4',100,3,NULL),(104,'G Star MEN','','6.70',100,3,NULL),(105,'G Star MEN','','4.5',100,3,NULL),(106,'G Star MEN','','4.5',100,3,NULL),(107,'G Star MEN','','4.5',100,3,NULL),(108,'G Star MEN','','6.00',100,3,NULL),(109,'G Star MEN','','6.00',100,3,NULL),(110,'G Star MEN','','6.00',100,3,NULL),(111,'G Star MEN','','6.50',100,3,NULL),(112,'G Star MEN','','6.50',100,3,NULL),(113,'G Star MEN','','6.50',100,3,NULL),(114,'G Star MEN','','7.50',100,3,NULL),(115,'H&M Shirts','Short-sleeved shirt in woven cotton fabric with a turn-down collar...Â COMPOSITIONÂ Cotton 100%','7.25',100,2,NULL),(116,'H&M Shirts','Shirt in woven cotton fabric with a button-down collar. Â COMPOSITIONÂ Cotton 100%','10.90',100,2,NULL),(117,'H&M Shirts','Shirt in chambray woven cotton fabric with a button-down collar, classic button placket, and open chest pocket. COMPOSITIONcotton 100%','9.60',100,2,NULL),(118,'H&M Shirts','Checked shirt in woven cotton fabric with a button-down collar. COMPOSITIONcotton 100%','10.29',100,2,NULL),(119,'H&M Shirts','Checked shirt in oxford cotton with a button-down collar. Â COMPOSITIONÂ Cotton 100%','11.81',100,2,NULL),(120,'H&M Shirts','Checked shirt in soft cotton flannel with a button-down collar. Â COMPOSITIONÂ Cotton 100%','10.48',100,2,NULL),(121,'H&M Shirts','Shirt in woven cotton fabric with a turn-down collarÂ COMPOSITIONÂ Cotton 100%','11.30',100,2,NULL),(122,'H&M Shirts','Checked shirt in woven cotton fabric with a turn-down collar, buttons without placket, and yoke at back. Â COMPOSITIONÂ Cotton 100%','11.62',100,2,NULL),(123,'H&M Shirts','Shirt in a woven TencelÂ® lyocell and cotton blend. Â COMPOSITIONÂ Cotton 52%, Lyocell 48%','12.40',100,2,NULL),(124,'H&M Shirts','Straight-cut shirt in washed denim with a collar. COMPOSITIONCotton 100%','11.05',100,2,NULL),(125,'H&M Shirts','Shirt in cotton denim with a cutaway collar. COMPOSITIONcotton 100%','11.41',100,2,NULL),(126,'H&M Shirts','Shirt in an airy linen and cotton blend. Â COMPOSITIONÂ Cotton 70% Linen 30% ','11.40',100,2,NULL),(127,'H&M Shirts','Straight-cut, gently tapered shirt in oxford cotton. Â COMPOSITIONÂ Cotton 100%','11.25',100,2,NULL),(128,'H&M Shirts','Shirt in oxford cotton with a button-down collar, classic button placket, and open chest pocket. Â COMPOSITIONÂ Cotton 100%, ','9.95',100,2,NULL),(129,'H&M Shirts','Plaid shirt in a soft viscose and cotton blend. COMPOSITIONÂ Cotton 56%, Viscose 44%','11.65',100,2,NULL),(130,'H&M Shirts','Plaid shirt in a soft viscose and cotton blend. COMPOSITIONÂ Cotton 56%, Viscose 44%','11.65',100,2,NULL),(131,'H&M Shirts','Plaid shirt in a soft viscose and cotton blend. COMPOSITIONÂ Cotton 56%, Viscose 44%','11.65',100,2,NULL),(132,'H&M Shirts','Straight-cut, gently tapered shirt in lightweight cotton flannelÂ COMPOSITIONÂ Cotton 100%','10.86',100,2,NULL),(133,'H&M Shirts','PREMIUM QUALITY. Shirt in soft, lightweight flannel made from premium cottonÂ COMPOSITIONÂ Cotton 100%, ','12.60',100,2,NULL),(134,'H&M Shirts','Straight-cut, relaxed-fit shirt in airy, woven cotton fabric with a band collar. Â COMPOSITIONÂ Cotton 100%','11.22',100,2,NULL),(135,'H&M Shirts','Straight-cut, gently tapered shirt in denim. COMPOSITIONÂ Linen 52%, Cotton 48%','10.07',100,2,NULL),(136,'H&M Shirts','Straight-cut, gently tapered shirt in denim. COMPOSITIONÂ Linen 52%, Cotton 48%','10.07',100,2,NULL),(137,'H&M Shirts','Straight-cut, gently tapered shirt in denim. COMPOSITIONÂ Linen 52%, Cotton 48%','10.07',100,2,NULL),(138,'H&M Shirts','CONSCIOUS. Shirt in organic cotton oxford with a button-down collar. COMPOSITIONcotton 100%','11.43',100,2,NULL),(139,'H&M Shirts','Short-sleeved shirt in soft, woven fabric made from a viscose and cotton blend with a printed pattern. Â COMPOSITIONÂ Cotton 61%, Viscose 39%','11.05',100,2,NULL),(140,'H&M Shirts','Short-sleeved shirt in a woven cotton blend with a printed pattern. COMPOSITIONÂ Viscose 57%, Polyester 43%','9.94',100,2,NULL),(141,'H&M Shirts','Striped shirt in airy oxford cotton fabric.Â COMPOSITIONÂ Cotton 100%','10.30',100,2,NULL),(142,'H&M Shirts','Shirt in woven cotton twill. Â COMPOSITIONÂ Cotton 100%, ','9.82',100,2,NULL),(143,'H&M Shirts','Shirt in airy, woven viscose fabric with a turn-down collar.COMPOSITIONViscose 100%','8.77',100,2,NULL),(144,'H&M Shirts','Shirt in airy, woven viscose fabric with a turn-down collar.COMPOSITIONViscose 100%','8.77',100,2,NULL),(145,'H&M Shirts','Shirt in airy, woven viscose fabric with a turn-down collar.COMPOSITIONViscose 100%','8.77',100,2,NULL),(146,'H&M Shirts','Straight-cut shirt in woven viscose fabric..Â COMPOSITIONÂ Viscose 100%','13.52',100,2,NULL),(147,'Nightwear for Women','Nightwear for Women','4.50',100,2,NULL),(148,'Nightwear for Women','Nightwear for Women','3.80',100,2,NULL),(149,'Nightwear for Women','Nightwear for Women','3.80',100,2,NULL),(150,'Nightwear for Women','Nightwear for Women','4.80',100,2,NULL),(151,'Nightwear for Women','Nightwear for Women','3.40',100,2,NULL),(152,'Nightwear for Women','Nightwear for Women','3.40',100,2,NULL),(153,'Nightwear for Women','Nightwear for Women','3.40',100,2,NULL),(154,'Nightwear for Women','Nightwear for Women','4.25',100,2,NULL),(155,'Nightwear for Women','Nightwear for Women','4.25',100,2,NULL),(156,'Nightwear for Women','Nightwear for Women','4.25',100,2,NULL),(157,'Nightwear for Women','Nightwear for Women','4.25',100,2,NULL),(158,'Nightwear for Women','Nightwear for Women','4.25',100,2,NULL),(159,'Nightwear for Women','Nightwear for Women','4.25',100,2,NULL),(160,'Nightwear for Women','Nightwear for Women','5.05',100,2,NULL),(161,'Nightwear for Women','Nightwear for Women','5.05',100,2,NULL),(162,'Nightwear for Women','Nightwear for Women','5.05',100,2,NULL),(163,'Nightwear for Women','Nightwear for Women','5.05',100,2,NULL),(164,'Nightwear for Women','Nightwear for Women','5.05',100,2,NULL),(165,'Nightwear for Women','Nightwear for Women','5.05',100,2,NULL),(166,'Nightwear for Women','Nightwear for Women','5.05',100,2,NULL),(167,'Nightwear for Women','Nightwear for Women','4.60',100,2,NULL),(168,'Nightwear for Women','Nightwear for Women','4.60',100,2,NULL),(169,'Nightwear for Women','Nightwear for Women','4.60',100,2,NULL),(170,'Nightwear for Women','Nightwear for Women','4.60',100,2,NULL),(171,'Nightwear for Women','Nightwear for Women','4.60',100,2,NULL),(172,'Nightwear for Women','Nightwear for Women','4.60',100,2,NULL),(173,'Nightwear for Women','Nightwear for Women','4.60',100,2,NULL),(174,'Nightwear for Women','Nightwear for Women','4.60',100,2,NULL),(175,'Nightwear for Women','Nightwear for Women','3.00',100,2,NULL),(176,'Nightwear for Women','Nightwear for Women','2.85',100,2,NULL),(177,'Nightwear for Women','Nightwear for Women','2.85',100,2,NULL),(178,'Nightwear for Women','Nightwear for Women','3.80',100,2,NULL),(179,'Nightwear for Women','Nightwear for Women','3.80',100,2,NULL),(180,'Nightwear for Women','Nightwear for Women','3.80',100,2,NULL),(181,'Nightwear for Women','Nightwear for Women','3.80',100,2,NULL),(182,'Nightwear for Women','Nightwear for Women','3.80',100,2,NULL),(183,'Nightwear for Women','Nightwear for Women','3.35',100,2,NULL),(184,'Nightwear for Women','Nightwear for Women','3.35',100,2,NULL),(185,'Nightwear for Women','Nightwear for Women','3.35',100,2,NULL),(186,'Nightwear for Women','Nightwear for Women','3.35',100,2,NULL),(187,'Nightwear for Women','Nightwear for Women','3.35',100,2,NULL),(188,'Nightwear for Women','Nightwear for Women','3.35',100,2,NULL),(189,'Nightwear for Women','Nightwear for Women','3.35',100,2,NULL),(190,'Nightwear for Women','Nightwear for Women','3.80',100,2,NULL),(191,'Nightwear for Women','Nightwear for Women','3.80',100,2,NULL),(192,'Nightwear for Women','Nightwear for Women','3.80',100,2,NULL),(193,'Nightwear for Women','Nightwear for Women','4.95',100,2,NULL),(194,'Nightwear for Women','Nightwear for Women','4.95',100,2,NULL),(195,'Nightwear for Women','Nightwear for Women','4.95',100,2,NULL),(196,'Nightwear for Women','Nightwear for Women','4.95',100,2,NULL),(198,'Nightwear for Women','Nightwear for Women','4.95',100,2,NULL),(199,'Nightwear for Women','Nightwear for Women','5.05',100,2,NULL),(200,'Nightwear for Women','Nightwear for Women','5.05',100,2,NULL),(201,'Nightwear for Women','Nightwear for Women','5.05',100,2,NULL),(202,'Nightwear for Women','Nightwear for Women','4.80',100,2,NULL),(203,'Nightwear for Women','Nightwear for Women','5.25',100,2,NULL),(204,'Nightwear for Women','Nightwear for Women','5.25',100,2,NULL),(205,'Nightwear for Women','Nightwear for Women','5.25',100,2,NULL),(206,'Nightwear for Women','Nightwear for Women','5.25',100,2,NULL),(207,'Nightwear for Women','Nightwear for Women','5.00',100,2,NULL),(208,'Short Pant','chambray, pocket print','8.00',100,2,NULL),(209,'Short Pant','chambray, pocket print','8.00',100,2,NULL),(210,'Short Pant','Short Pant','9.00',100,2,NULL),(211,'Short Pant','Short Pant','7.5 - 8.5',100,2,NULL),(212,'Short Pant','Short Pant','8.00',100,2,NULL),(213,'Short Pant','Short Pant','6 - 7',100,2,NULL),(214,'Short Pant','Short Pant','7.50',100,2,NULL),(215,'Short Pant','Short Pant','7.5',100,2,NULL),(216,'Short Pant','Short Pant','5 - 6',100,2,NULL),(217,'Short Pant','Short Pant','10.00',100,2,NULL),(218,'Short Pant','Short Pant','10.00',100,2,NULL),(219,'Short Pant','Short Pant','7.50',100,2,NULL),(220,'Short Pant','Short Pant','9.00',100,2,NULL),(221,'Short Pant','Short Pant','7.50',100,2,NULL),(222,'Short Pant','Short Pant','8.25',100,2,NULL),(223,'Short Pant','Short Pant','6.50',100,2,NULL),(224,'Short Pant','Short Pant','7.75',100,2,NULL),(225,'Short Pant','Short Pant','7.00',100,2,NULL),(226,'Short Pant','Short Pant','11 - 12.50',100,2,NULL),(227,'Short Pant','Short Pant','10 - 12',100,2,NULL),(228,'Short Pant','Short Pant','13 - 14',100,2,NULL),(229,'Short Pant','Short Pant','6.50',100,2,NULL),(230,'Short Pant','Short Pant with Drawing','6.50',100,2,NULL),(231,'Short Pant','Short Pant ','5.50 - 6.50',100,2,NULL),(232,'Short Pant','Short Pant','7 - 8',100,2,NULL),(233,'Short Pant','Short Pant','7.50',100,2,NULL);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sub_category`
--

DROP TABLE IF EXISTS `sub_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `sub_category` (
  `id` int(11) NOT NULL,
  `name` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sub_category`
--

LOCK TABLES `sub_category` WRITE;
/*!40000 ALTER TABLE `sub_category` DISABLE KEYS */;
INSERT INTO `sub_category` VALUES (1,'Shirt (Men)'),(2,'Shirt (Women) '),(3,'T Shirt'),(4,'Winter'),(5,'Kids'),(6,'Nightwear'),(7,'Short Pants'),(8,'Boys');
/*!40000 ALTER TABLE `sub_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(110) NOT NULL,
  `password` varchar(110) DEFAULT NULL,
  `name` varchar(110) DEFAULT NULL,
  `address` varchar(1010) DEFAULT NULL,
  `telephone` varchar(110) DEFAULT NULL,
  `email` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (0,'0',NULL,NULL,NULL,NULL,NULL),(1,'admin','1234','','','','');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-30  2:09:04
