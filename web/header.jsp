<%-- 
    Document   : header
    Created on : Apr 23, 2019, 3:42:03 AM
    Author     : md_al
--%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.ourshop.connection.Database"%>
<%
Database db = new Database();
            db.connect();
            //try{
%>
<!--================Header Menu Area =================-->
    <header class="header_area">
      <div class="top_menu">
        <div class="container">
          <div class="row">
            <div class="col-lg-7">
              <div class="float-left">
                <%--<p>Phone: +01 256 25 235</p>
                <p>email: info@eiser.com</p>--%>
              </div>
            </div>
            
          </div>
        </div>
      </div>
      <div class="main_menu">
        <div class="container">
          <nav class="navbar navbar-expand-lg navbar-light w-100">
            <!-- Brand and toggle get grouped for better mobile display -->
            <a class="navbar-brand logo_h" href="home.jsp">
              <img src="img/logo.png" alt="" />
            </a>
            <button
              class="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div
              class="collapse navbar-collapse offset w-100"
              id="navbarSupportedContent"
            >
              <div class="row w-100 mr-0">
                <div class="col-lg-12 pr-0">
                  <ul class="nav navbar-nav center_nav pull-right">
                    <li class="nav-item">
                      <a class="nav-link" href="home.jsp">Home</a>
                    </li>
                    <%if(session.getAttribute("username")!=null && session.getAttribute("username").equals("admin")){%>
                        <li class="nav-item">
                          <a class="nav-link" href="products.jsp">All Products</a>
                        </li>
                    <%}%>
                    <%
                    Statement stC2 = db.connection.createStatement();
                                                            String qC2 = "select * from category";
                                                            ResultSet rsC2 = stC2.executeQuery(qC2);
                                                            while(rsC2.next()){
                    %>
                    <li class="nav-item submenu dropdown">
                    <a href="products.jsp?categoryId=<%=rsC2.getString("id")%>" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                      aria-expanded="false"><%=rsC2.getString("name")%></a>
                    <ul class="dropdown-menu">
                      <%
                        Statement stC4 = db.connection.createStatement();
                                                            String qC4 = "select * from sub_category";
                                                            ResultSet rsC4 = stC4.executeQuery(qC4);
                                                            while(rsC4.next()){
                        %>
                      
                      <li class="nav-item">
                        <a class="nav-link" href="products.jsp?categoryId=<%=rsC2.getString("id")%>&subCategoryId=<%=rsC4.getString("id")%>"><%=rsC4.getString("name")%></a>
                      </li>
                      <%}%>
                    </ul>
                  </li>
                    
                    <%}%>
                    <%if(session.getAttribute("username")==null){%>
                    <li class="nav-item">
                      <a class="nav-link" href="loginRegister.jsp">Login/Register</a>
                    </li>
                    <%}else{%>
                    <%if(session.getAttribute("username").equals("admin")){%>
                    <li class="nav-item">
                      <a class="nav-link" href="addProduct.jsp">Add Product</a>
                    </li>
                    <%}%>
                    <li class="nav-item">
                      <a class="nav-link" href="loginRegister.jsp">Logout(<%=session.getAttribute("username")%>)</a>
                    </li>
                    <%}%>
                  </ul>
                </div>

                <%--<div class="col-lg-5 pr-0">
                  <ul class="nav navbar-nav navbar-right right_nav pull-right">
                    <li class="nav-item">
                      <a href="#" class="icons">
                        <i class="ti-search" aria-hidden="true"></i>
                      </a>
                    </li>

                    <li class="nav-item">
                      <a href="#" class="icons">
                        <i class="ti-shopping-cart"></i>
                      </a>
                    </li>

                    <li class="nav-item">
                      <a href="#" class="icons">
                        <i class="ti-user" aria-hidden="true"></i>
                      </a>
                    </li>

                    <li class="nav-item">
                      <a href="#" class="icons">
                        <i class="ti-heart" aria-hidden="true"></i>
                      </a>
                    </li>
                  </ul>
                </div>--%>
              </div>
            </div>
          </nav>
        </div>
      </div>
    </header>
    <!--================Header Menu Area =================-->
