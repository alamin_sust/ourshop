<%-- 
    Document   : home.jsp
    Created on : Apr 23, 2019, 1:56:23 AM
    Author     : md_al
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <link rel="icon" href="img/favicon.png" type="image/png" />
  <title>Our Shop</title>
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="css/bootstrap.css" />
  <link rel="stylesheet" href="vendors/linericon/style.css" />
  <link rel="stylesheet" href="css/font-awesome.min.css" />
  <link rel="stylesheet" href="css/themify-icons.css" />
  <link rel="stylesheet" href="css/flaticon.css" />
  <link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css" />
  <link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css" />
  <link rel="stylesheet" href="vendors/nice-select/css/nice-select.css" />
  <link rel="stylesheet" href="vendors/animate-css/animate.css" />
  <link rel="stylesheet" href="vendors/jquery-ui/jquery-ui.css" />
  <!-- main css -->
  <link rel="stylesheet" href="css/style.css" />
  <link rel="stylesheet" href="css/responsive.css" />
  <script type = "text/javascript">
          function displayNextImage() {
              x = (x === images.length - 1) ? 0 : x + 1;
              document.getElementById("img").src = images[x];
          }

          function displayPreviousImage() {
              x = (x <= 0) ? images.length - 1 : x - 1;
              document.getElementById("img").src = images[x];
          }

          function startTimer() {
              setInterval(displayNextImage, 5000);
          }

          var images = [], x = -1;
          images[0] = "img/banner/banner-bg.jpg";
          images[1] = "img/banner/banner-bg2.jpg";
          images[2] = "img/banner/banner-bg3.jpg";
    </script>
</head>

<body onload = "startTimer()">
  <%@ include file="header.jsp" %>

  <!--================Home Banner Area =================-->
  <%--<section class="home_banner_area mb-40">
    <div class="banner_inner d-flex align-items-center">
      <div class="container">
        <div class="banner_content row">
          <div class="col-lg-12">
            <p class="sub text-uppercase">men Collection</p>
            <h3><span>Show</span> Your <br />Personal <span>Style</span></h3>
            <h4>Fowl saw dry which a above together place.</h4>
            <a class="main_btn mt-40" href="products.jsp?categoryId=2">View Collection</a>
          </div>
        </div>
      </div>
    </div>
  </section>--%>
  
  
  
      <div class="col-lg-12">
          <img id="img" style="width: 100%;" src="img/banner/banner-bg.jpg"/>
          <%--<button type="button" onclick="displayPreviousImage()">Previous</button>
          <button type="button" onclick="displayNextImage()">Next</button>--%>
      </div>
  
  <!--================End Home Banner Area =================-->

  <!-- Start feature Area -->
  <section class="feature-area section_gap_bottom_custom">
    <div class="container">
      <div class="row">
        <div class="col-lg-3 col-md-6">
          <div class="single-feature">
            <a href="#" class="title">
              <i class="flaticon-money"></i>
              <h3>Money back gurantee</h3>
            </a>
            <p>Shall open divide a one</p>
          </div>
        </div>

        <div class="col-lg-3 col-md-6">
          <div class="single-feature">
            <a href="#" class="title">
              <i class="flaticon-truck"></i>
              <h3>Free Delivery</h3>
            </a>
            <p>Shall open divide a one</p>
          </div>
        </div>

        <div class="col-lg-3 col-md-6">
          <div class="single-feature">
            <a href="#" class="title">
              <i class="flaticon-support"></i>
              <h3>Alway support</h3>
            </a>
            <p>Shall open divide a one</p>
          </div>
        </div>

        <div class="col-lg-3 col-md-6">
          <div class="single-feature">
            <a href="#" class="title">
              <i class="flaticon-blockchain"></i>
              <h3>Secure payment</h3>
            </a>
            <p>Shall open divide a one</p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End feature Area -->

  <!--================ Feature Product Area =================-->
  <section class="feature_product_area section_gap_bottom_custom">
    <div class="container">
        <%if (session.getAttribute("sM") != null) {%>
        <div class="alert alert-success text-center">
            <%=session.getAttribute("sM")%>
        </div>
        <%session.setAttribute("sM", null);
            }%>
        <%if (session.getAttribute("eM") != null) {%>
        <div class="alert alert-danger text-center">
            <%=session.getAttribute("eM")%>
        </div>
        <%session.setAttribute("eM", null);
            }%>
      <div class="row justify-content-center">
        <div class="col-lg-12">
          <div class="main_title">
            <h2><span>Featured product</span></h2>
          </div>
        </div>
      </div>
        <div class="row">
<%
                 Statement stP = db.connection.createStatement();
                                                            String qP = "select * from product where id>0 order by id desc limit 30" ;
                                                            ResultSet rsP = stP.executeQuery(qP);
                                                            while(rsP.next()) {
                 
                 %>
      
        <div class="col-lg-3 col-md-6">
          <div class="single-product">
            <div class="product-img">
                <img onclick="location.href='productDetails.jsp?productId=<%=rsP.getString("id")%>'" 
                     class="img-fluid w-100" src="img/products/<%=rsP.getString("id")%>.jpg" alt="" style="cursor: pointer;" />
                <%if(session.getAttribute("username")!=null && session.getAttribute("username").equals("admin")){%>
                <div class="p_icon">
                <a href="addProduct.jsp?productId=<%=rsP.getString("id")%>&update=true">
                  Update
                </a>
                <a href="addProduct.jsp?deleteProductId=<%=rsP.getString("id")%>">
                  Delete
                </a>
                <%--<a href="#">
                  <i class="ti-heart"></i>
                </a>
                <a href="#">
                  <i class="ti-shopping-cart"></i>
                </a>--%>
              </div>
              <%}%>
            </div>
            <div class="product-btm">
              <a href="#" class="d-block">
                <h4><%=rsP.getString("name")%></h4>
              </a>
              <div class="mt-3">
                <span class="mr-4">$<%=rsP.getString("price")%></span>
                <del>$<%=rsP.getString("price")%></del>
              </div>
            </div>
          </div>
        </div>
<%}%>
        
      </div>
    </div>
  </section>
  <!--================ End Feature Product Area =================-->

  <%@ include file="footer.jsp" %>
</body>

</html>